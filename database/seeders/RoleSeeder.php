<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            'Cliente','Administrador'
        ];
        foreach($array as $data){
            $role = new Role();
            $role->name = $data;
            $role->save();
        }

    }
}
