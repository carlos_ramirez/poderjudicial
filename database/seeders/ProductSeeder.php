<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $array = [
            ['Producto 1','descripcion...',123.45,5,50],
            ['Producto 2','descripcion...',45.65,15,75],
            ['Producto 3','descripcion...',39.73,12,122],
            ['Producto 4','descripcion...',250,8,80],
            ['Producto 5','descripcion...',59.35,10,104],
        ];
        foreach($array as $data){
            $product = new Product();
            $product->name = $data[0];
            $product->description = $data[1];
            $product->precio = $data[2];
            $product->impuesto = $data[3];
            $product->stock = $data[4];
            $product->save();
        }



    }
}
