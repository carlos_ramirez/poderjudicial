<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Shop;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
        //mostrar lista de venta
        $shops = Shop::select(
            'shops.id as idShop',
            'users.name as nameUser',
            'products.name as nameProduct',
            'products.impuesto as impuesto',
            'products.precio as precio',
            'shops.total_product as cantidad',
            'shops.total_shop as total',
        )
            ->join('users','users.id','shops.id_user')
            ->join('products','products.id','shops.id_product')
            ->get();

        return view('Admin/indexShop',compact('shops'));
    }

    public function store(Request $request)
    {
        $impuesto = $request->tax / 100;
        $costoTotalVenta = $request->cu * $request->cantidad;

        $impuestoSobreVentaTotal = $costoTotalVenta * $impuesto;
        $venta = $impuestoSobreVentaTotal + $costoTotalVenta;

        $shop = new Shop;
        $shop->id_user = Auth::user()->getAuthIdentifier();
        $shop->id_product = $request->id_product;
        $shop->total_product = $request->cantidad;
        $shop->total_shop = $venta;
        $shop->save();

        $product = Product::find($request->id_product);
        $product->stock = $product->stock - $request->cantidad;
        $product->save();

        return redirect('product/'.$request->id_product);
    }
}
