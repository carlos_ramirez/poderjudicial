@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Productos</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <ul>
                            <li class="nav-item"> {{ $product['name'] }} --- Existencia: {{ $product['stock'] }}</li>
                        </ul>


                            <form class="form-edit-add" role="form" action="{{ route('shop.store')}}"
                                  method="POST" enctype="multipart/form-data" autocomplete="off">

                                @csrf

                                <div class="row">
                                    <div class="col-md-8">
                                        <div class="panel panel-bordered">
                                            <div class="panel-body">

                                                <input type="hidden" name="id_product" value="{{$product['id']}}">
                                                <input type="hidden" name="cu" value="{{$product['precio']}}">
                                                <input type="hidden" name="tax" value="{{$product['impuesto']}}">

                                                <div class="form-group">
                                                    <label for="name">Precio por unidad {{$product['precio']}}</label>
                                                    <input type="number" class="form-control" id="cantidad" name="cantidad" placeholder="cantidad"
                                                           value=" ">
                                                </div>

                                                <button type="submit" class="btn btn-primary pull-right save">
                                                    Guardar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
