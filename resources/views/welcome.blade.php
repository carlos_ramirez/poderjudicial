@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Lista de productos</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <ul>
                            @foreach($products as $data)
                                <li class="nav-item"> {{ $data['name'] }} --- Existencia: {{ $data['stock'] }} --- Precio c/u {{ $data['precio'] }}

                            @if( \Illuminate\Support\Facades\Auth::check())
                                        <a class="nav-link" href="{{ route('product.show',[ 'id'=>$data['id'] ] )}}">ver/compar</a>
                                @else
                                        <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                @endif

                            </li>
                            @endforeach
                        </ul>



                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
