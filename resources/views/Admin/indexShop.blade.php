@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Lista de ventas</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <div class="table-responsive">
                                <table id="dataTable" class="table table-hover" width="100%">
                                    <thead>
                                    <tr>
                                        <th>Cliente</th>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                        <th>Total</th>
                                        <th>Factura</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @foreach($shops as $data)
                                            <tr>

                                                    <td>{{ $data['nameUser'] }}</td>
                                                    <td>{{ $data['nameProduct'] }}</td>
                                                    <td>{{ $data['cantidad'] }}</td>
                                                    <td>{{ $data['total'] }}</td>
                                                    <td>
                                                        <!-- Button trigger modal -->
                                                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal{{$data['idShop']}}">
                                                         # {{ $data['idShop'] }}
                                                        </button>
                                                        <!-- Modal -->
                                                        <div class="modal fade" id="exampleModal{{$data['idShop']}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h5 class="modal-title" id="exampleModalLabel">Detalle factura</h5>
                                                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                    </div>
                                                                    <div class="modal-body">
                                                                        <lu>
                                                                            <li>Cliente: {{ $data['nameUser'] }}</li>
                                                                            <li>Producto: {{ $data['nameProduct'] }}</li>
                                                                            <li>Cantidad: {{ $data['cantidad'] }}</li>
                                                                            <li>Precio unitario: {{ $data['precio'] }}</li>
                                                                            <li>Impuesto: {{ $data['impuesto'] }}</li>
                                                                            <li>Total: {{ $data['total'] }}</li>
                                                                        </lu>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                            </tr>
                                    @endforeach
                                    </tbody>
                                </table>


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
